# from model import users
from flask import Flask, request
from controller import price_table
from controller import sanitize_inputs
from model import users
import json
from flasgger import Swagger, swag_from

app = Flask(__name__)
app.config["SWAGGER"] = {"title": "Swagger-UI", "uiversion": 2}
Swagger(app) #http://localhost:5000/apidocs/index.html

@app.route("/generate_price_table", methods=["POST"])
@swag_from("swagger/calculate_price_table.yml")
def calculate_price_table():
    try:
        data = request.get_json()
        clean_data = sanitize_inputs.sanitize_price_table_input(data)
        if clean_data != True:
            return clean_data
        user = users.User(data['username'], data['passwd'])
        if user.user_id == False:
            return "Username and password combination not found."
        table = price_table.calculate_price_table(
            rate=data["rate"],
            nper=data["nper"],
            pv=data["pv"]
        )
        if data["register"]:
            user.register_price_table(json.dumps(
                table), rate=data["rate"], nper=data["nper"], pv=data["pv"])
        return table
    except Exception as E:
        print(E)
        return "Unable to create price table. Please, check the documentation and make sure the inputs are consistent."

@app.route("/get_price_table", methods=["POST"])
@swag_from("swagger/get_price_table.yml")
def get_price_table():
    try:
        data = request.get_json()
        user = users.User(data['username'], data['passwd'])
        if user.user_id == False:
            return "Username and password combination not found."
        return user.price_table[0]
    except Exception as E:
        print(E)
        return "Unable to fetch price table. Please, check the documentation and make sure the inputs are consistent."

@app.route("/renegotiate", methods=["POST"])
@swag_from("swagger/renegotiate.yml")
def recalculate_table():
    try:
        data = request.get_json()
        clean_data = sanitize_inputs.sanitize_renegotiate_input(data)
        if clean_data != True:
            return clean_data
        user = users.User(data['username'], data['passwd'])
        if user.user_id == False:
            return "Username and password combination not found."
        new_table = price_table.recalculate_price_table(
            price_table = user.price_table[0],
            rate = user.price_table[1],
            nper = user.price_table[2],
            pv = user.price_table[3],
            last_installment_paid = data["last_installment_paid"])
        if data["register"]:
            user.register_price_table(json.dumps(
                new_table), rate = user.price_table[1],
                nper = user.price_table[2],
                pv = user.price_table[3]
            )
        return new_table
    except Exception as E:
        print(E)
        return "Unable to recalculate price table. Please, check the documentation and make sure the inputs are consistent."

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
