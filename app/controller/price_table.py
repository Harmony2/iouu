import pandas as pd
import numpy as np
import json

def calculate_price_table(rate, nper, pv, ndecimals=2, output="dict"):
    acceptable_outputs = ["dict", "dataframe"]
    if output not in acceptable_outputs:
        return "Output not acceptable.\nPlease, select between dict or dataframe"
    payment = round(np.pmt(rate, nper, pv), ndecimals) * -1
    
    all_values = [] if output == "dataframe" else {}
    for i in range(1, nper+1):
        fees = round(rate * pv, ndecimals)
        amortization = round(payment - fees, ndecimals)
        pv -= round(amortization, ndecimals)
        if output == "dataframe":
            all_values.append([fees, amortization, payment, pv])
        else:
            all_values[i] = {
                'fees': fees,
                'amortization': amortization,
                'payment': payment,
                'present_value': pv
            }
    if output == 'dataframe' : return pd.DataFrame(all_values, columns=['fees', 'amortization', 'payment','pv'])
    else : return all_values

def convert_table_to_df(price_table):
    data = []
    index = []
    for key in list(price_table.keys()):
        data.append(price_table[key])
        index.append(int(key))
    df = pd.DataFrame(data, columns=["amortization","fees","payment","present_value"], index=index)
    return df

def recalculate_price_table(price_table, rate, nper, pv, last_installment_paid):
    price_table = json.loads(price_table)
    df = convert_table_to_df(price_table)
    if last_installment_paid+1 >= len(df):
        return "Debt was paid in full."
    df = df[last_installment_paid:]
    print(df)
    print(len(df))
    df.index = [i for i in range(1, len(df)+1)]
    return calculate_price_table(rate, nper - last_installment_paid, pv - df["present_value"][len(df)-1])
