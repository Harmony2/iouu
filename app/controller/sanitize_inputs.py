def sanitize_price_table_input(data):
    if sorted(list(data.keys())) != ['nper', 'passwd', 'pv', 'rate', 'register', 'username']:
        return {'Error': 'Invalid Request Body. Wrong number of arguments. Check the documentation.'}

    for k in ['nper', 'rate', 'pv']:
        if (type(data[k]) != int) & (type(data[k]) !=  float):
            return {'Error': 'Invalid Body Request. Invalid data type for key {%s: %s}' % (k, data[k])}
        elif data[k] < 0:
            return {'Error': 'Invalid Body Request. Negative value: %s' % data[k]}

    for k in ['username', 'passwd']:
        if type(data[k]) != str:
            return {"Error": "Invalid Body Request. Username and Password should be strings."}

    if type(data['register']) != bool:
        return {"Eror": "Invalid Body Request. Register flag should be a boolean"}

    return True

def sanitize_renegotiate_input(data):
    if sorted(list(data.keys())) != ['last_installment_paid', 'passwd', 'register', 'username']:
        return {'Error': 'Invalid Request Body. Wrong number of arguments. Check the documentation.'}
        
    if type(data['last_installment_paid']) != int:
        return {'Error': 'Invalid Body Request. Invalid data type for key last_installment_paid.'}

    for k in ['username', 'passwd']:
        if type(data[k]) != str:
            return {"Error": "Invalid Body Request. Username and Password should be strings."}

    if type(data['register']) != bool:
        return {"Error": "Invalid Body Request. Register flag should be a boolean."}
    
    return True