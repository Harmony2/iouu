import requests

def generate_table():
    url = "http://localhost:5000/"
    j = {
        'username': 'admin',
        'passwd': 'admin123',
        'rate': 0.07,
        'nper': 10,
        'pv': 10000,
        'register': False
    }
    r = requests.post(url, json=j)
    if list(r.json().keys()) == ['%s' % i  for i in range(1,11)]:
        return True
    else:
        return False

def generate_and_register_table():
    url = "http://localhost:5000/"
    j = {
        'username': 'admin',
        'passwd': 'admin123',
        'rate': 0.07,
        'nper': 10,
        'pv': 10000,
        'register': True
    }
    r = requests.post(url, json=j)
    if list(r.json().keys()) == ['%s' % i  for i in range(1,11)]:
        return True
    else:
        return False

def generate_renegotiation():
    url = "http://localhost:5000/renegotiate"
    j = {
        "username": "admin",
        "passwd": "admin123",
        "last_installment_paid": 8,
        "register": False
    }
    r = requests.post(url, json=j)
    if list(r.json().keys()) == ['%s' % i for i in range(1,3)]:
        return True
    else:
        return False

def generate_and_register_renegotiation():
    url = "http://localhost:5000/renegotiate"
    j = {
        "username": "admin",
        "passwd": "admin123",
        "last_installment_paid": 8,
        "register": False
    }
    r = requests.post(url, json=j)
    if list(r.json().keys()) == ['%s' % i for i in range(1,3)]:
        return True
    else:
        return False

print("Starting Test Routine")
if not generate_table():
    print("[-] Route / failed for price table generation.")
else:
    print("[+] Route / Succesfull for price table generation!")

if not generate_and_register_table():
    print("[-] Route / failed for price table generation and registration.")
else:
    print("[+] Route / sucessful for price table generation and registration!")

if not generate_renegotiation():
    print("[-] Route /renegotiate failed for price table generation.")
else:
    print("[+] Route /renegotiate sucessful for price table generation!")

if not generate_and_register_renegotiation():
    print("[-] Route /renegotiate failed for price table generation and registration.")
else:
    print("[+] Route /renegotiate succesful for price table generation and registration!")

print("Test Routine Finished")