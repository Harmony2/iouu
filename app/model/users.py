from clients.mysql import Mysql

class User():
    def __init__(self, username, passwd):
        self.mysql = Mysql()
        self.username = ''.join(e for e in username if e.isalnum())
        self.passwd = ''.join(e for e in passwd if e.isalnum())
        self.user_id = self.__fetch_user_id()
        self.price_table = self.fetch_price_table()

    def __fetch_user_id(self):
        query = f"""
        select
            user_id
        from
            users
        where
            username = '{self.username}'
            AND passwd = '{self.passwd}';
        """
        self.mysql.cursor.execute(query)
        try:
            return self.mysql.cursor.fetchone()[0]
        except:
            self.mysql.cursor.fetchall()
            return False

    def fetch_price_table(self):
        query = f"""
        select
            price_table,
            rate,
            nper,
            pv
        from
            price_tables
        where
            user_id = {self.user_id}
            order by registration_date desc;
        """
        self.mysql.cursor.execute(query)
        try:
            return self.mysql.cursor.fetchone()
        except:
            return 'User has no Price Table registered'

    def register_price_table(self, price_table, rate, nper, pv):
        self.mysql.cursor.fetchall()
        query = f"""
        insert into price_tables (
            user_id,
            price_table,
            rate,
            nper,
            pv,
            registration_date
        ) values (
            {self.user_id},
            '{price_table}',
            {rate},
            {nper},
            {pv},
            NOW()
        );
        """
        self.mysql.cursor.execute(query)
        self.mysql.db.commit()
        self.price_table = self.fetch_price_table()