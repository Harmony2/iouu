import mysql.connector
from config import MYSQL

class Mysql():
    def __init__(self):
        self.hostname = MYSQL.mysql_conf["hostname"]
        self.user = MYSQL.mysql_conf["user"]
        self.password = MYSQL.mysql_conf["password"]
        self.port = MYSQL.mysql_conf["port"]
        self.db = self.__connect_database()
        self.cursor = self.db.cursor()
        try:
            self.cursor.execute("use appdb;")
        except:
            self.__initiate_databse()

    def __connect_database(self):
        db = mysql.connector.connect(
            host=self.hostname,
            user=self.user,
            password=self.password,
            port=self.port
        )
        return db
    
    def __initiate_databse(self):
        queries = ["""CREATE database appdb;""",
        """USE appdb;""",
        """create table users (
            user_id int NOT NULL AUTO_INCREMENT,
            username varchar(255) NOT NULL,
            passwd varchar(255) NOT NULL,
            PRIMARY KEY (user_id)
        );""", 
        """create table price_tables (
            table_id int NOT NULL AUTO_INCREMENT,
            user_id int NOT NULL,
            price_table varchar(2048) NOT NULL,
            rate float NOT NULL,
            nper int NOT NULL,
            pv float NOT NULL,
            registration_date DATETIME NOT NULL,
            PRIMARY KEY (table_id),
            FOREIGN KEY (user_id) REFERENCES users(user_id)
        );""", 
        """insert into users (
            username,
            passwd
        ) values (
            "admin",
            "admin123"
        );
        """]
        from pprint import pprint
        for q in queries:
            print(q)
            self.cursor.execute(q)
            self.db.commit()
            self.cursor.close()
            self.cursor = self.db.cursor()
        self.db.commit()