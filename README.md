# Desafio IOUU

Desenvolvimento de uma Tabela Price para financiamento

## Inicializando o Banco de dados (Docker)

Para rodar em local host, precisaremos iniciar um banco de dados alcançável pela aplicação. Isso pode ser facilmente realizado com Docker utilizando o seguinte comando

    docker run --name iouu-database -e MYSQL_ROOT_PASSWORD=123456 -ti -p 3306:3306 mysql:5.7

## API

### Instalando Dependências

Após isso, podemos instalar as dependências de nossa API no ambiente local. Isso pode ser realizado utilizando

    pip install -r requirements.txt

<b>a partir da pasta /app.</b>

### Inicializando a Aplicação

Podemos facilmente inicializar a aplicação rodando, a <b>partir da pasta app</b>, o arquivo main.py

    python main.py

Caso necessário, utilize o <i>Alias</i> python3

    python3 main.py

## Configurações de banco

Caso necessário, podemos alterar as configurações de banco para apontar para um host diferente, em outra porta. Para isso basta alterar o arquivo <b>/app/config/MYSQL.py</b> que tem o seguinte conteúdo:

    mysql_conf = {
        "hostname": "localhost",
        "user": "root",
        "password": "123456",
        "port": 3306
    }

Garanta que o servidor contendo a API enxergue e alcance o <i>Database</i>.

## Swagger

Ao provisionar corretamente a API, sua documentação de uso pode ser encontrada em http://localhost:5000/apidocs/#/
Se necessário, basta trocar <i>localhost</i> pelo IP ou Hostname correto (servidor na qual a API foi provisionada).